#include <ESP32Servo.h>

//from https://github.com/mcxiaoke/ESPDateTime/blob/master/examples/simple/main.cpp
#include <Arduino.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <Update.h>

#include "ESPDateTime.h"
#include "config.h"


const char* host = "esp32";
const char* ssid = "Nicholls2g";
const char* password = "Asid2354";
int LED_BUILTIN = 2;
const int timeHoursOffset=13;//add hours to gmt
const int openTimeHours=7;
const int closeTimeHours=19;

WebServer server(80);

char statusJson[]="Not set";

unsigned long lastMs = 0;
unsigned long ms = millis();
/*
 * Login page
 */

const char* loginIndex =
"<style>    body {line-height: 150%; background-color: black;color:rgb(17, 216, 17);font-family: Lucida Console, Courier, Lucida Sans, verdana;font-size:14px}a {color:     #68FFC8;}a:hover {color: #68dcff;}a:active{color: white;}a:visited{color:#46dc44;}  </style>"
"<body>"
"<form name='loginForm'>"
    "<table width='20%' bgcolor='A09F9F' align='center'>"
        "<tr>"
            "<td colspan=2>"
                "<center><font size=4><b>Cat Door ESP32 Login Page</b></font></center>"
                "<br>"
            "</td>"
            "<br>"
            "<br>"
        "</tr>"
        "<tr>"
             "<td>Username:</td>"
             "<td><input type='text' size=25 name='userid'><br></td>"
        "</tr>"
        "<br>"
        "<br>"
        "<tr>"
            "<td>Password:</td>"
            "<td><input type='Password' size=25 name='pwd'><br></td>"
            "<br>"
            "<br>"
        "</tr>"
        "<tr>"
            "<td><input type='submit' onclick='check(this.form)' value='Login'></td>"
        "</tr>"
    "</table>"
"</form>"
"<script>"
    "function check(form) {"
    "  if(form.userid.value=='admin' && form.pwd.value=='Asid2354'){"
    "    window.open('/serverIndex')"
    "  }else{"
    "    alert('Error Password or Username')/*displays error message*/"
    "  }"
    "}"
"</script>"
"</body>"
"";

/*
 * Server Index Page
 */

const char* serverIndex =
"<body>"
"<style>    body {line-height: 150%; background-color: black;color:rgb(17, 216, 17);font-family: Lucida Console, Courier, Lucida Sans, verdana;font-size:14px}a {color:     #68FFC8;}a:hover {color: #68dcff;}a:active{color: white;}a:visited{color:#46dc44;}  </style>"
"<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
"<h1>Cat Door - V1.1</h1><br>"
"<a href='' onclick=\"return runCmd('openexit');\">Open Exit</a> |"
"<a href='' onclick=\"return runCmd('closeexit');\">Close Exit</a> |"
"<a href='' onclick=\"return runCmd('getStatus');\">Last Time check</a> |"
 "<div id='res'></div>"

"<hr>Upload a new binary."
"<form method='POST' action='#' enctype='multipart/form-data' id='upload_form'>"
   "<input type='file' name='update'>"
        "<input type='submit' value='Update'>"
    "</form>"
 "<div id='prg'>progress: 0%</div>"
 "<script>"
  "$('form').submit(function(e){"
  "e.preventDefault();"
  "var form = $('#upload_form')[0];"
  "var data = new FormData(form);"
  " $.ajax({"
  "url: '/update',"
  "type: 'POST',"
  "data: data,"
  "contentType: false,"
  "processData:false,"
  "xhr: function() {"
  "var xhr = new window.XMLHttpRequest();"
  "xhr.upload.addEventListener('progress', function(evt) {"
  "if (evt.lengthComputable) {"
  "var per = evt.loaded / evt.total;"
  "$('#prg').html('progress: ' + Math.round(per*100) + '%');"
  "}"
  "}, false);"
  "return xhr;"
  "},"
  "success:function(d, s) {"
     "console.log('OTA success!');"
     "$('#res').append('OTA update success<br>');"
   "},"
   "error: function (a, b, c) {"
     "console.log('OTA failed.');"
     "$('#res').append('OTA update failed<br>');"
   "}"
 "});"
 "});"
 "function runCmd(cmd){$.get({url:'/'+cmd,success:function(data){$('#res').append(data+'<br>');}});return false;}"
 "</script>"
 "</body>"
 "";



void setupWiFi() {
  // Connect to WiFi network
  WiFi.begin(ssid, password);
  Serial.print("Milliseconds:");

  Serial.println(millis());
  Serial.print("WiFi Connecting...");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(200);
    Serial.print(WiFi.status());
  }
  if(WiFi.status() == WL_CONNECTED){
    Serial.print("\nWiFi connected\n");
  }
  Serial.println("");
   
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("--------");
}

int sumHours(int hours1, int hours2){
    int sum = (hours1 + hours2) % 24;
    return sum < 0 ? 24 + sum : +sum;
}

void setupDateTime() {
  Serial.printf("Setup datetime\n");

  // setup this after wifi connected
  // you can use custom timeZone,server and timeout
  // DateTime.setTimeZone(-4);
  //   DateTime.setServer("asia.pool.ntp.org");
  //   DateTime.begin(15 * 1000);
  DateTime.setServer("time.pool.aliyun.com");//incorrect time
  
  //DateTime.setServer("nz.pool.ntp.org");//correct time
  //DateTime.setTimeZone("CST-8");
  //DateTime.setTimeZone("NZDT");
  DateTime.setTimeZone("GMT");
  
/*
NZDT  New Zealand Daylight Time   UTC +13
NZST  New Zealand Standard Time   UTC +12
*/
  
  DateTime.begin();
  if (!DateTime.isTimeValid()) {
    Serial.println("Failed to get time from server, restart.");
    ESP.restart();
  } else {
    Serial.printf("Date Now is %s\n", DateTime.toISOString().c_str());
    Serial.printf("Timestamp is %ld\n", DateTime.now());
  }
}
 
Servo myservo1;  // create servo object to control a servo
Servo myservo2;  // create servo object to control a servo
// 16 servo objects can be created on the ESP32
 
int pos = 0;    // variable to store the servo position
// Recommended PWM GPIO pins on the ESP32 include 2,4,12-19,21-23,25-27,32-33 
int servo1Pin = 12;
int servo2Pin = 13;
bool allowExitEnabled=false;
bool allowEntryEnabled=false;
bool denyExitEnabled=false;
bool denyEntryEnabled=false;

void showTime() {
  Serial.printf("TimeZone:      %s\n", DateTime.getTimeZone());
  Serial.printf("Up     Time:   %lu seconds\n", millis() / 1000);
  Serial.printf("Boot   Time:   %ld seconds\n", DateTime.getBootTime());
  Serial.printf("Cur    Time:   %ld seconds\n",
                DateTime.getBootTime() + millis() / 1000);
  Serial.printf("Now    Time:   %ld\n", DateTime.now());
  Serial.printf("OS     Time:   %ld\n", DateTime.osTime());
  Serial.printf("NTP    Time:   %ld\n", DateTime.ntpTime(2 * 1000L));
  // Serial.println();
  Serial.printf("Local  Time:   %s\n",
                DateTime.format(DateFormatter::SIMPLE).c_str());
  Serial.printf("ISO86  Time:   %s\n", DateTime.toISOString().c_str());
  Serial.printf("UTC    Time:   %s\n",
                DateTime.formatUTC(DateFormatter::SIMPLE).c_str());
  Serial.printf("UTC86  Time:   %s\n",
                DateTime.formatUTC(DateFormatter::ISO8601).c_str());

  Serial.println("===========");
  time_t t = time(NULL);
  Serial.printf("OS local:     %s", asctime(localtime(&t)));
  Serial.printf("OS UTC:       %s", asctime(gmtime(&t)));

  //time_t timeNow=DateTime.getTime();
  
  DateTimeParts timeNow = DateTime.getParts();
  int curHours = timeNow.getHours();
  int curMins = timeNow.getMinutes();

  Serial.printf("read hours curHours:       %d\n",curHours);
  
  
  //time server issue - out by 11 hours
  //curHours=sumHours(curHours,11);//add 11, wrap around
  //fix hours
  curHours+=timeHoursOffset;  //read 4am, actually 5pm, so increase time 
  if(curHours>23){
    curHours=curHours-23;
  }

  sprintf(statusJson,"Last time: %d:%d (GMT Time: %s)\n",curHours,curMins,DateTime.format(DateFormatter::SIMPLE).c_str());
  Serial.printf("fixed hours curHours:      %d\n",curHours);
  
  //lock exit door
  denyExit();  
  if(curHours>=openTimeHours && curHours<closeTimeHours+1){//19
     
    if(curHours==openTimeHours&&curMins>30){
      //open door to allow exit from inside
      Serial.print("after 730am\n");
      allowExit();  
    //} else if(curHours==19&&curMins<=30){
    /*
    } else if(curHours==closeTimeHours){//&&curMins<=30){
      //open door to allow exit from inside
      Serial.print("before 6pm\n");
      allowExit();  
    */
    } else
    {
      Serial.print("between 7am and 730pm\n");
      allowExit();  
    }
  }
  //if(true){//timeNow.getHours()>6&&timeNow.getMinutes()>30  || timeNow.getHours()<19&&timeNow.getMinutes()<30){
  /*
  if(timeNow.getHours()>6 && timeNow.getHours()<20){
    //open door allow entry from outside
    allowEntry();  
  }else{
    //lock door, not entry from outside
    denyEntry();  
  }
  */
}

void allowExit(){
  Serial.print("call allowexit\n");
  
  if(!allowExitEnabled){
    Serial.print("allowexit is not enabled\n");
    //move servo
    allowExitEnabled=true;
    denyExitEnabled=false;
    //myservo1.write(0);    // tell servo to go to position in variable 'pos'
    openOutsideServo();
  }else{
    Serial.print("allowexit is enabled\n");
  }
}  
void denyExit(){
  Serial.print("call denyexit\n");
  if(!denyExitEnabled){
    Serial.print("denyexit is not enabled\n");
    //move servo
    denyExitEnabled=true;
    allowExitEnabled=false;
    //myservo1.write(180);    // tell servo to go to position in variable 'pos'
    closeOutsideServo();
  }else{
    Serial.print("denyexit is enabled\n");
  }
}  
void allowEntry(){
  if(!allowEntryEnabled){
    //move servo
    allowEntryEnabled=true;
    denyEntryEnabled=false;
    //myservo1.write(0);    // tell servo to go to position in variable 'pos'
    openInsideServo();
  }
}  
void denyEntry(){
    if(!denyEntryEnabled){
    //move servo
    denyEntryEnabled=true;
    allowEntryEnabled=false;
    //myservo2.write(180);    // tell servo to go to position in variable 'pos'
    closeInsideServo();
  }
}  
 
void slowBlink(){  
  digitalWrite(LED_BUILTIN, HIGH);
  delay(2000);
  digitalWrite(LED_BUILTIN, LOW);
  delay(100);
}

//called on startup
void fastBlink(){  
  for(int sc=0;sc++;sc<=3){
    digitalWrite(LED_BUILTIN, HIGH);
    delay(300);
    digitalWrite(LED_BUILTIN, LOW);
    delay(100);
  }
}

void closeInsideServo(){
  Serial.print("close inside\n");
  for (pos = 0; pos <= 97; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo1.write(pos);    // tell servo to go to position in variable 'pos'
    delay(5);             // waits 15ms for the servo to reach the position
  }
}

void openInsideServo(){
  Serial.print("open inside\n");
  
  for (pos = 90; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
    myservo1.write(pos);    // tell servo to go to position in variable 'pos'
    delay(5);             // waits 15ms for the servo to reach the position
  }
}

void openOutsideServo(){
  Serial.print("open outside\n");
  for (pos = 95; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
    myservo2.write(pos);    // tell servo to go to position in variable 'pos'
    delay(5);             // waits 15ms for the servo to reach the position
  }
}

void closeOutsideServo(){
  Serial.print("close outside\n");
  for (pos = 0; pos <= 90; pos += 5) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo2.write(pos);    // tell servo to go to position in variable 'pos'
    delay(5);             // waits 15ms for the servo to reach the position
  }
}

void testServos(){
  for (pos = 0; pos <= 180; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo1.write(pos);    // tell servo to go to position in variable 'pos'
    myservo2.write(pos);    // tell servo to go to position in variable 'pos'
    delay(5);             // waits 15ms for the servo to reach the position
  }
  for (pos = 180; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
    myservo1.write(pos);    // tell servo to go to position in variable 'pos'
    myservo2.write(pos);    // tell servo to go to position in variable 'pos'
    delay(5);             // waits 15ms for the servo to reach the position
  }
 
}



/*
 * setup function
 */
void setup(void) {
  // Allow allocation of all timers
  ESP32PWM::allocateTimer(0);
  ESP32PWM::allocateTimer(1);
  ESP32PWM::allocateTimer(2);
  ESP32PWM::allocateTimer(3);
  myservo1.setPeriodHertz(50);    // standard 50 hz servo
  myservo1.attach(servo1Pin, 500, 2400); // attaches the servo on pin 12 to the servo object
  myservo2.setPeriodHertz(50);    // standard 50 hz servo
  myservo2.attach(servo2Pin, 500, 2400); // attaches the servo on pin 13 to the servo object
  // using default min/max of 1000us and 2000us
  // different servos may require different min/max settings
  // for an accurate 0 to 180 sweep


  pinMode (LED_BUILTIN, OUTPUT);
  fastBlink();  //blink blue light, has a delay

  //delay(1000);
  Serial.begin(115200);

  setupWiFi();

  /*use mdns for host name resolution*/
  if (!MDNS.begin(host)) { //http://esp32.local
    Serial.println("Error setting up MDNS responder!");
    while (1) {
      delay(1000);
    }
  }
  
  
  
  Serial.println("mDNS responder started");
  /*return index page which is stored in serverIndex */
  server.on("/", HTTP_GET, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/html", loginIndex);
  });
  server.on("/serverIndex", HTTP_GET, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/html", serverIndex);
  });
 
  server.on("/getStatus", HTTP_GET, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/html", statusJson);
  });
  server.on("/openexit", HTTP_GET, []() {
    server.sendHeader("Connection", "close");
    openOutsideServo();
    server.send(200, "text/html", "open exit OK");
  });
  
  server.on("/closeexit", HTTP_GET, []() {
    server.sendHeader("Connection", "close");
    closeOutsideServo();
    server.send(200, "text/html", "close exit OK");
  });
  
  
  /*handling uploading firmware file */
  server.on("/update", HTTP_POST, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
    ESP.restart();
  }, []() {
    HTTPUpload& upload = server.upload();
    if (upload.status == UPLOAD_FILE_START) {
      Serial.printf("Update: %s\n", upload.filename.c_str());
      if (!Update.begin(UPDATE_SIZE_UNKNOWN)) { //start with max available size
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_WRITE) {
      /* flashing firmware to ESP*/
      if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_END) {
      if (Update.end(true)) { //true to set the size to the current progress
        Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
      } else {
        Update.printError(Serial);
      }
    }
  });
  server.begin();

  setupDateTime();

  /*
  Serial.println(DateTime.now());
  Serial.println("--------------------");
  Serial.println(DateTime.toString());
  Serial.println(DateTime.toISOString());
  Serial.println(DateTime.toUTCString());
  Serial.println("--------------------");
  Serial.println(DateTime.format(DateFormatter::COMPAT));
  Serial.println(DateTime.format(DateFormatter::DATE_ONLY));
  Serial.println(DateTime.format(DateFormatter::TIME_ONLY));
  Serial.println("--------------------");
  DateTimeParts p = DateTime.getParts();
  Serial.printf("%04d/%02d/%02d %02d:%02d:%02d %ld (%s)\n", p.getYear(),
                p.getMonth(), p.getMonthDay(), p.getHours(), p.getMinutes(),
                p.getSeconds(), p.getTime(), p.getTimeZone());
  Serial.println("--------------------");

  closeInsideServo();
  openInsideServo();
  closeOutsideServo();
  openOutsideServo();
  */

  testServos();
  showTime();

}
void checkForSerialCommands(){
 if(Serial.available())  { // if there is data comming
    String command = Serial.readStringUntil('\n'); // read string until meet newline character

    if(command == "ON") {
      digitalWrite(LED_BUILTIN, HIGH); // turn on LED
      Serial.println("LED is turned ON"); // send action to Serial Monitor
    }else if(command == "OFF")    {
      digitalWrite(LED_BUILTIN, LOW);  // turn off LED
      Serial.println("LED is turned OFF"); // send action to Serial Monitor
    }else if(command == "OPENEXIT")    {
      openOutsideServo();
    }else if(command == "CLOSEEXIT")    {
      closeOutsideServo();
    }else if(command == "TIME")    {
      showTime();
    }
  }  
}


void loop(void) {
  server.handleClient();
  delay(1);

  //
  checkForSerialCommands();

  //blink every 5 sec
  if (millis() - ms > 8 * 1000L) {
    slowBlink();
  }
  //check every 15 mins
  if (millis() - ms > 15 *60 * 1000L) {
    ms = millis();
    Serial.println("--------------------");
    if (!DateTime.isTimeValid()) {
      Serial.println("Failed to get time from server, restart.");
      //DateTime.begin();
      //on error, lock cats in
      closeOutsideServo();

      ESP.restart();
    } else {
      showTime();
    }
  }
}
